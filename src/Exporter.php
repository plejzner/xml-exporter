<?php

declare(strict_types=1);

namespace App;

use Redis;

class Exporter
{
    public function __construct(
        private Redis $redis,
        private bool $verbose
    ) {}

    public function export(string $filePath): void
    {
        if (false === $xml = simplexml_load_file($filePath)) {
            throw new AppException('error parsing xml file ' . $filePath);
        }

        foreach ($xml->children() as $child) {
            if ($child->getName() === 'subdomains') {
                $subdomainsArr = json_decode(json_encode($child), true);
                $this->setKey('subdomains', json_encode($subdomainsArr['subdomain']));
            }
            if ($child->getName() === 'cookies') {
                foreach ($child->children() as $cookie) {
                    $key = 'cookie';
                    foreach ($cookie->attributes() as $attribute) {
                        $key .= ':' . (string)$attribute;
                    }
                    $this->setKey($key, (string)$cookie);
                }
            }
        }
    }

    private function setKey(string $key, string $value): void
    {
        if ($this->redis->set($key, $value) === false) {
            throw new AppException('redis write error');
        }
        if ($this->verbose === true) {
            echo $key . "\n";
        }
    }
}
