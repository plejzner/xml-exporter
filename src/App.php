<?php

declare(strict_types=1);

namespace App;

use Redis;

class App
{
    public function export(string $filePath, bool $verbose = false): void
    {
        if (file_exists($filePath) === false) {
            throw new AppException('file does not exist: ' . $filePath);
        }

        $redis = new Redis();
        if ($redis->connect(getenv('REDIS_HOST')) === false) {
            throw new AppException('redis connection failed');
        }
        $exporter = new Exporter($redis, $verbose);

        $exporter->export($filePath);
    }
}
