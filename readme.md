### development

install dependencies: `docker-compose run php composer install`

run services: `docker-compose up -d` (it will run 'redis-serv' as only service that needs to be up)

run exporter: `docker-compose run exporter -v /app/var/config.xml`

run tests: `docker-compose run phpunit`

### deploy

build exporter image: `docker build -t xml-exporter -f docker/deploy/Dockerfile .`

run image as container: `docker run --env REDIS_HOST='...' xml-exporter -v /path/to/file.xml`