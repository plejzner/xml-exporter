#!/usr/bin/env php
<?php

declare(strict_types=1);

use App\App;

require 'vendor/autoload.php';

$verbose = false;
$options = getopt("v");
if (isset($options['v'])) {
  $verbose = true;
}

if ($argc < 2 || $verbose === true && $argc < 3) {
  throw new Exception('file path is required');
}
$path = $argv[$argc-1];

$app = new App();
$app->export($path, $verbose);