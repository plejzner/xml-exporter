<?php

declare(strict_types=1);

use App\App;
use App\AppException;
use PHPUnit\Framework\TestCase;

class ExportTest extends TestCase
{
    private const PROPER_TEST_XML_FILE_PATH = __DIR__ . '/' . 'config_for_tests_proper.xml';
    
    private const MALFORMED_TEST_XML_FILE_PATH = __DIR__ . '/' . 'config_for_tests_malformed.xml';

    private static Redis $redis;

    public static function setUpBeforeClass(): void
    {
        self::$redis = new Redis();
        if (self::$redis->connect(getenv('REDIS_HOST')) === false) {
            throw new Exception('redis connection failed');
        }
    }

    public function setUp(): void
    {
        self::$redis->flushDb();
    }

    public function tearDown(): void
    {
        self::$redis->flushDb();
    }

    public function test_there_should_be_proper_data_in_redis_after_exporting_from_proper_XML_file(): void
    {
        $expectedData = [
            'subdomains' => ['http://secureline.tools.avast.com', 'http://gf.tools.avast.com'],
            'cookie:dlp-avast:amazon' => 'mmm_amz_dlp_777_ppc_m',
            'cookie:dlp-avg:computerbuild' => 'mmm_cbd_dlp_779_ppc_m',
        ];

        $app = new App();

        $app->export(self::PROPER_TEST_XML_FILE_PATH);

        $actualSubdomains = json_decode(self::$redis->get('subdomains'));
        $actualCookie1 = self::$redis->get('cookie:dlp-avast:amazon');
        $actualCookie2 = self::$redis->get('cookie:dlp-avg:computerbuild');

        self::assertEquals($expectedData['subdomains'][0], $actualSubdomains[0]);
        self::assertEquals($expectedData['subdomains'][1], $actualSubdomains[1]);
        self::assertEquals($expectedData['cookie:dlp-avast:amazon'], $actualCookie1);
        self::assertEquals($expectedData['cookie:dlp-avg:computerbuild'], $actualCookie2);
    }

    public function test_App_should_throw_exception_if_malformed_xml_file_provided()
    {
        $this->expectException(AppException::class);

        $app = new App();

        // suppress simplexml notices
        $errorLevel = error_reporting();
        error_reporting(E_ERROR);

        $app->export(self::MALFORMED_TEST_XML_FILE_PATH);

        error_reporting($errorLevel);
    }

    public function test_App_should_throw_exception_if_wrong_filepath_provided(): void
    {
        $this->expectException(AppException::class);

        $app = new App();

        $app->export('wrong/path');
    }
}
